package Utils;

import java.util.Random;

public class GeradorDeVeiculo {

	public static String gerarPlaca() {
		int nrAleatorio;
		int letraAleatorio;
		int i;

		String[] ArrayInteiro = { "1","2","3","4", "5", "6", "7", "8", "9" };
		String[] ArrayString = {"A","B", "C", "D", "E", "F", "G", "H", "I",
				"J", "L", "M", "N", "O", "P", "U", "V", "X", "W", "Y", "K", "Z" };
		
		String nome = "";
		String placa = "";

		Random random = new Random();

		for (i = 0; i <= 2; i++) {

			nrAleatorio = 0 + random.nextInt(8);
			letraAleatorio = 0 + random.nextInt(3);
			nome = ArrayInteiro[nrAleatorio] + ArrayString[letraAleatorio];
			placa = placa + "" + nome;
		}
		
		return placa;

	}
	
	/*public static void main(String[] args){
		System.out.println(gerarPlaca());
	}
*/
	
}
