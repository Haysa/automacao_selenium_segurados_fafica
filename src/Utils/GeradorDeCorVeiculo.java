package Utils;

import java.util.Random;

public class GeradorDeCorVeiculo {

	public static String gerarCor() {

		int nrCoresAleatorio;
		int i;

		String[] ArrayCor = { "AMARELO", "AZUL", "VERDE", "VERMELHO", "PRETO",
				"CINZA", "ROXO", "LARANJA", "ROSA", "BEGE", "DOURADO",
				"PRATEADO" };

		String nome = "";
		String cor = "";

		Random random = new Random();

		for (i = 0; i <= 1; i++) {

			nrCoresAleatorio = 0 + random.nextInt(8);
			nome = ArrayCor[nrCoresAleatorio];
			cor = nome + "" + nome;

		}

		return cor;

	}

	public static void main (String [] args){
		System.out.println(gerarCor());
	}
}
