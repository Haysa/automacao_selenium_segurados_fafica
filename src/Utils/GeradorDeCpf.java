package Utils;

public class GeradorDeCpf {
	
	/*/
	 * gerando os d�gitos do cpf
	 */	
	private static String calcDigVerif(String num) {    
       
		Integer primeiroDigito; 
        Integer segundoDigito;    
        int soma = 0; 
        int peso = 10; 
        
        for (int i = 0; i < num.length(); i++)    
                soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;    
    
        		if (soma % 11 == 0 | soma % 11 == 1)    
        			primeiroDigito = new Integer(0);    
        		else    
        			primeiroDigito = new Integer(11 - (soma % 11));    
    
        		soma = 0;    
        		peso = 11; 
        		
        for (int i = 0; i < num.length(); i++)    
                soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;    
            
        soma += primeiroDigito.intValue() * 2;    
        if (soma % 11 == 0 | soma % 11 == 1)    
            segundoDigito = new Integer(0);    
        else    
            segundoDigito = new Integer(11 - (soma % 11));    
    
        return primeiroDigito.toString() + segundoDigito.toString();    
    }    
    
	/*
	 * gerando cpf
	 */
	public static String geraCPF() {   
    	
        String iniciais = "";    
        Integer numero;  
        
        for (int i = 0; i < 9; i++) {    
            numero = new Integer((int) (Math.random() * 10));    
            iniciais += numero.toString();    
        }    
        return iniciais + calcDigVerif(iniciais);    
    }    
    
    /*
     * Validando cpf 
     */
    public static boolean validaCPF(String cpf) {    
        if (cpf.length() != 11)    
            return false;    
    
        String numDig = cpf.substring(0, 9);    
        return calcDigVerif(numDig).equals(cpf.substring(9, 11));    
    }   
      
    public static void main (String args[]){  
        System.out.println(geraCPF());  
        //System.out.println(validaCPF(geraCPF()));  
    }  
	
}
