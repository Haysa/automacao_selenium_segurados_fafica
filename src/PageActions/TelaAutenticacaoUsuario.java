package PageActions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Setup.Setup;

public class TelaAutenticacaoUsuario extends Setup {
	
	//static By nomeUsuario = driver.findElement(html/body/div[1]/div[3]/form/input[2]);

	public void nomeUsuario(String email) {
		driver.findElement(By.xpath("html/body/div[1]/div[3]/form/input[2]"))
				.sendKeys(email);
	}

	public void senhaUsuario(String senha) {
		driver.findElement(By.xpath("html/body/div[1]/div[3]/form/input[3]"))
				.sendKeys(senha);
	}

	public void clicarBntEntrar() {
		driver.findElement(By.xpath("html/body/div[1]/div[3]/form/a")).click();

	}

}
