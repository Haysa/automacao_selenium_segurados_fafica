package PageActions;

import org.openqa.selenium.By;
import org.testng.Assert;

import Setup.Setup;
import Utils.GeradorDeCpf;
import Utils.GeradorDeNome;

public class TelaFormCadastroCliente extends Setup {

	static By campoNome = By.xpath(".//*[@id='form-cadastro']/div[1]/input[3]");
	static By campoCpf = By.xpath(".//*[@id='form-cadastro']/div[1]/input[4]");
	static By campoDataNascimento = By.name("datanascimento");
	static By escolhaDataNascimento = By.xpath(".//*[@id='ui-datepicker-div']/table/tbody/tr[3]/td[1]/a");
	static By campoRg = By.xpath(".//*[@id='form-cadastro']/div[1]/input[6]");
	static By campoOrgaoExpedidor = By.xpath(".//*[@id='form-cadastro']/div[1]/input[7]");
	static By campoCnh = By.xpath(".//*[@id='form-cadastro']/div[1]/input[8]");
	static By campoCategoria = By.xpath(".//*[@id='form-cadastro']/div[1]/select[1]");
	static By campoEstadoCivil = By.name("estadocivil");
	static By campoTelefoneUm = By.xpath(".//*[@id='form-cadastro']/div[1]/input[9]");
	static By campoEmail = By.xpath(".//*[@id='form-cadastro']/div[1]/input[11]");
	static By campoCep = By.xpath(".//*[@id='form-cadastro']/div[2]/input[1]");
	static By campoSituacaoResidencia = By.xpath(".//*[@id='situacaores']");
	static By campoLogradouro = By.xpath(".//*[@id='logradouro']");
	static By campoNumero = By.xpath(".//*[@id='numero']");
	static By campoBairro = By.xpath(".//*[@id='bairro']");
	static By campoEstado = By.xpath(".//*[@id='uf']");
	static By campoCidade = By.xpath(".//*[@id='localidade']");
	static By campoNaturalidade = By.xpath(".//*[@id='form-cadastro']/div[2]/input[8]");
	static By btnSalvarEnovo = By.xpath(".//*[@id='toolbar-box']/div/div[1]/ul/li[1]/a");
	static By btnSalvarEsair = By.xpath(".//*[@id='toolbar-box']/div/div[1]/ul/li[2]/a");
	static By btnCancelar = By.xpath(".//*[@id='toolbar-box']/div/div[1]/ul/li[3]/a");
	
	
	/*
	 * M�todos:
	 */

	public void preencherNome() {

		driver.findElement(campoNome).sendKeys(GeradorDeNome.gerarNome());
	}

	public void preencherCpf() {

		driver.findElement(campoCpf).sendKeys(GeradorDeCpf.geraCPF());
	}

	public void dataNascimento(String data) {
		driver.findElement(campoDataNascimento).sendKeys(data);

	}

	public void escolherDataNascimento() {
		driver.findElement(escolhaDataNascimento).click();
	}

	public void preencherRg() {

		driver.findElement(campoRg).sendKeys(GeradorDeCpf.geraCPF());
	}

	public void preencherOrgaoExpedidor(String orgao) {
		driver.findElement(campoOrgaoExpedidor).sendKeys(orgao);
	}

	public void preencherCNH() {
		driver.findElement(campoCnh).sendKeys(GeradorDeCpf.geraCPF());
	}

	public void selecioneCategoria() {
		driver.findElement(campoCategoria).sendKeys("A");

	}

	public void selecionarEstadoCivil() {
		driver.findElement(campoEstadoCivil).sendKeys("Solteiro");

	}

	public void preencherTelefoneUm(String fone) {
		driver.findElement(campoTelefoneUm).sendKeys(fone);
	}

	public void preencherEmail(String email) {
		driver.findElement(campoEmail).sendKeys(email);
	}

	public void preencherCep(String cep) {
		driver.findElement(campoCep).sendKeys(cep);
	}

	public void escolherSituacaoResidencia() {
		driver.findElement(campoSituacaoResidencia).sendKeys("P");

	}

	public void preencherLogradouro(String logradouro) {
		driver.findElement(campoLogradouro).sendKeys(logradouro);
	}

	public void preencherNumero(String numero) {
		driver.findElement(campoNumero).sendKeys(numero);
	}

	public void preencherBairro(String bairro) {
		driver.findElement(campoBairro).sendKeys(bairro);
	}

	public void preencherEstado(String estado) {
		driver.findElement(campoEstado).sendKeys(estado);
	}

	public void preencherCidade(String cidade) {
		driver.findElement(campoCidade).sendKeys(cidade);
	}

	public void preencherNaturalidade(String naturalidade) {
		driver.findElement(campoNaturalidade).sendKeys(naturalidade);
	}

	public void clicarBtnSalvarEnovo() {
		driver.findElement(btnSalvarEnovo).click();
	}

	public void clicarBtnSalvarEsair() {
		driver.findElement(btnSalvarEsair).click();
	}

	public void clicarBtnCancelar() {
		driver.findElement(btnCancelar).click();
	}
	
	public void validaCadastroCliente(){
		String retorno1;
		String retorno2;
		String espera;
		
		retorno1 = driver.findElement(By.id("alert")).getText();
		retorno2 = driver.findElement(By.id("alertSucesso")).getText();
		espera = retorno1 + retorno2;
		Assert.assertEquals(espera, "Sucesso! cliente salvo com sucesso!");
	}

}
