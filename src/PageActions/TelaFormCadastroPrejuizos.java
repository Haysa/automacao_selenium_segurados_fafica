package PageActions;

import org.openqa.selenium.By;

import Setup.Setup;

public class TelaFormCadastroPrejuizos extends Setup{

	static By campoDataOcorrencia = By.name("dataocorrencia");
	static By escolherDataOcorrencia = By.xpath(".//*[@id='ui-datepicker-div']/table/tbody/tr[2]/td[7]/a");
	static By campoTipoOcorrencia = By.name("tipoocorrencia");
	static By campoDescricao = By.name("descricao");
	static By campoCep = By.name("cep");
	static By campoRua = By.name("logradouro");
	static By campoBairro = By.name("bairro");
	static By campoEstado = By.name("estado");
	static By campoCidade = By.name("cidade");
	static By campoSituacaoVeiculo = By.name("situacaoautomovel");
	static By btnLocalizarVeiculo = By.linkText("javascript:openVeiculoContextoPrejuizo();");
	static By valorTotalPrejuizo = By.name("valortotal");
	static By valorTaxaPrejuizo = By.name("valor_taxa_prejuizo");
	static By btnSalvarEnovo = By.linkText("javascript:salvarNovo('form-cadastro')");
	
	public void preencherDataOcorrencia(){
		driver.findElement(campoDataOcorrencia).click();
	}
	
	public void escolherDataOcorrencia(){
		driver.findElement(escolherDataOcorrencia).click();
	}
	
	public void preencherTipoOcorrencia(String ocorrencia){
		driver.findElement(campoTipoOcorrencia).sendKeys(ocorrencia);
	}
	
	public void preencherCampoDescricao(){
		driver.findElement(campoDescricao).click();
	}
	
	public void preencherCampoCep(){
		driver.findElement(campoCep).sendKeys();
	}
	
	public void preencherCampoRua(){
		driver.findElement(campoRua).sendKeys();
	}
	
	public void preencherCampoBairro(){
		driver.findElement(campoBairro).sendKeys();
	}
	
	public void preencherCampoEstado(){
		driver.findElement(campoEstado).sendKeys();
	}
	
	public void preencherCampoCidade(){
		driver.findElement(campoCidade).sendKeys();
	}
	
	public void preencherSituacaoVeiculo(){
		driver.findElement(campoSituacaoVeiculo).sendKeys();
	}
}
	
