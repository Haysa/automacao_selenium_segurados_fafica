package PageActions;

import org.openqa.selenium.By;

import Setup.Setup;
import Utils.GeradorDeCorVeiculo;
import Utils.GeradorDeVeiculo;

public class TelaFormCadastroVeiculo extends Setup {

	static By tipoCarro = By.xpath(".//*[@id='form-cadastro']/div[1]/select");
	static By encontrarCampoPlaca = By.cssSelector("input[name=\"placa\"]");
	static By encontrarCampoMarca = By.cssSelector("input[name=\"marca\"]");
	static By encontrarCampoModelo = By.name("modelo");
	static By encontrarCampoCor = By.name("cor");
	static By encontrarCampoAnoModelo = By.name("ano");
	static By encontrarCampoAnoFab = By.name("anofabricacao");
	static By encontrarCampoChassi = By.cssSelector("input[name=\"chassi\"]");
	static By encontrarCampoRenavan = By.cssSelector("input[name=\"renavan\"]");
	static By encontrarCampoValor = By.name("valor");
	static By encontrarCampoValorFipe = By.name("valorfipe");
	static By encontrarBtnSalvarEnovo = By.xpath(".//*[@id='toolbar-box']/div/div[1]/ul/li[1]/a/span");
	static By encontrarBtnSalvarEsair = By.xpath(".//*[@id='toolbar-box']/div/div[1]/ul/li[2]/a");
	
	/**
	 * M�todos:
	 */
	
	public void selecionarTipoDeVeiculo(String tipo) {
		driver.findElement(tipoCarro).sendKeys(tipo);
	}

	public void preencherCampoPlaca() {
		driver.findElement(encontrarCampoPlaca).sendKeys(GeradorDeVeiculo.gerarPlaca());
	}

	public void preencherMarca() {
		driver.findElement(encontrarCampoMarca).sendKeys(GeradorDeVeiculo.gerarPlaca());
	}

	public void preencherModelo() {
		driver.findElement(encontrarCampoModelo).sendKeys(GeradorDeCorVeiculo.gerarCor());
	}

	public void preencherCor() {
		driver.findElement(encontrarCampoCor).sendKeys(GeradorDeCorVeiculo.gerarCor());
	}

	public void preencherAnoModelo(String anoModelo) {
		driver.findElement(encontrarCampoAnoModelo).sendKeys(anoModelo);
	}

	public void preencherAnoFabricacao(String anoFab) {
		driver.findElement(encontrarCampoAnoFab).sendKeys(anoFab);
	}

	public void preencherChassi() {
		driver.findElement(encontrarCampoChassi).sendKeys(GeradorDeVeiculo.gerarPlaca());
	}

	public void preencherRenavan() {
		driver.findElement(encontrarCampoRenavan)
				.sendKeys(GeradorDeVeiculo.gerarPlaca());
	}

	public void preencherValor(String valor) {
		driver.findElement(encontrarCampoValor).sendKeys(valor);
	}

	public void preencherValorFipe(String fipe) {
		driver.findElement(encontrarCampoValorFipe).sendKeys(fipe);
	}

	public void clicarBotaoSalvarEnovo() {
		driver.findElement(encontrarBtnSalvarEnovo).click();

	}

	public void clicarBotaoSalvarEsair() {
		driver.findElement(encontrarBtnSalvarEsair).click();
	}

}
