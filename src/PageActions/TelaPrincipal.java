package PageActions;

import org.openqa.selenium.By;

import Setup.Setup;

public class TelaPrincipal extends Setup {
	
	static By btnModuloCliente = By.xpath(".//*[@id='menu']/li[3]/a");
	static By btnIncluirNovoCliente = By.xpath(".//*[@id='menu']/li[3]/ul/li[1]/a");
	static By btnGerenciarCliente = By.xpath(".//*[@id='menu']/li[2]/ul/li[3]/a");
	static By btnModuloVeiculo = By.xpath(".//*[@id='menu']/li[4]/a");
	static By btnIncluirVeiculo = By.xpath(".//*[@id='menu']/li[4]/ul/li[1]/a");
	static By btnGerenciarVeiculo = By.xpath(".//*[@id='menu']/li[3]/ul/li[3]/a");
	static By btnModuloPrejuizos = By.xpath(".//*[@id='menu']/li[5]/a");
	static By btnRegistrarPrejuizo = By.linkText("/segurados/prejuizos_incluir");
	
	/*
	 * Metodos:
	 */
	
	public void clicarModuloClientes() {
		driver.findElement(btnModuloCliente).click();
	}

	public void clicarIncluirNovoCliente() {
		driver.findElement(btnIncluirNovoCliente).click();
	}

	public void clicarGerenciarClientes() {
		driver.findElement(btnGerenciarCliente).click();
	}

	public void clicarModuloVeiculo() {
		driver.findElement(btnModuloVeiculo).click();
	}

	public void clicarIncluirNovoVeiculo() {
		driver.findElement(btnIncluirVeiculo).click();
	}

	public void clicarGerenciarVeiculos() {
		driver.findElement(btnGerenciarVeiculo).click();
	}
	
	public void clicarModuloPrejuizos(){
		driver.findElement(btnModuloPrejuizos).click();
	}
	
	public void clicarIncluirNovoPrejuizo(){
		driver.findElement(btnRegistrarPrejuizo).click();
	}

}
