package Tests.Clientes;

import org.junit.Before;
import org.junit.Test;
//import org.openqa.selenium.internal.seleniumemulation.IsElementPresent;

import PageActions.TelaAutenticacaoUsuario;
import PageActions.TelaFormCadastroCliente;
import PageActions.TelaPrincipal;
import Setup.Setup;
import Utils.GeradorDeNome;

public class AdicionarClienteComSucesso extends Setup {

	TelaAutenticacaoUsuario login = new TelaAutenticacaoUsuario();
	TelaPrincipal home = new TelaPrincipal();
	TelaFormCadastroCliente cad = new TelaFormCadastroCliente();

	@Before
	public void preCondition() {
		login.nomeUsuario("teste@fafica.com");
		login.senhaUsuario("123");
		login.clicarBntEntrar();
	}

	@Test
	public void cadastrarCliente() throws InterruptedException {

		for (int i = 0; i < 100; i++) {
			home.clicarModuloClientes();
			home.clicarIncluirNovoCliente();
			cad.preencherNome();
			cad.preencherCpf();
			cad.dataNascimento("12/12/1998");
			Thread.sleep(50);
			cad.escolherDataNascimento();
			cad.preencherRg();
			cad.preencherOrgaoExpedidor("SDS/PE");
			cad.preencherCNH();
			cad.selecioneCategoria();
			cad.selecionarEstadoCivil();
			cad.preencherTelefoneUm("87654345679");
			cad.preencherEmail("teste@teste.com");
			cad.preencherCep("55.508-765");
			cad.escolherSituacaoResidencia();
			Thread.sleep(50);
			cad.preencherLogradouro("Rua l�dio bezerra cavalcante");
			cad.preencherNumero("12");
			cad.preencherBairro("boa vista");
			cad.preencherEstado("PE");
			cad.preencherCidade("Caruaru");
			cad.preencherNaturalidade("Natural");
			cad.preencherLogradouro("LALALA");
			cad.clicarBtnSalvarEsair();
			// cad.validaCadastroCliente();
			Thread.sleep(50);
			// }
		}
	}
}
