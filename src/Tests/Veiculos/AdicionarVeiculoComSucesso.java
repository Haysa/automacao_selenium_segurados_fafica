package Tests.Veiculos;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import PageActions.TelaAutenticacaoUsuario;
import PageActions.TelaFormCadastroVeiculo;
import PageActions.TelaPrincipal;
import Setup.Setup;

public class AdicionarVeiculoComSucesso extends Setup {

	@Test
	public void cadastrarVeiculo() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		TelaAutenticacaoUsuario login = new TelaAutenticacaoUsuario();
		TelaPrincipal home = new TelaPrincipal();
		TelaFormCadastroVeiculo cad = new TelaFormCadastroVeiculo();

		login.nomeUsuario("teste@fafica.com");
		login.senhaUsuario("123");
		login.clicarBntEntrar();
				
		for(int i = 0; i< 100; i++ ){
		home.clicarModuloVeiculo();
		home.clicarIncluirNovoVeiculo();
		cad.selecionarTipoDeVeiculo("CARRO");
		cad.preencherCampoPlaca();
		cad.preencherMarca();
		cad.preencherModelo();
		cad.preencherCor();
		cad.preencherAnoModelo("2010");
		cad.preencherAnoFabricacao("1998");
		cad.preencherChassi();
		cad.preencherRenavan();
		cad.preencherValor("100,00");
		cad.preencherValorFipe("100,00");
		cad.clicarBotaoSalvarEsair();
		Thread.sleep(2000);
		}
	}
}
